<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function index(){
        return view('web.register');
    }

    public function register(Request $request){
        // dd($request->all());
        $firstName = $request['fname'];
        $lastName = $request['lname'];
        $gender = $request['gender'];
        $nationality = $request['nationality'];
        $language = $request['bahasa'];
        $bio = $request['bio'];
        return view('web.welcome', compact('firstName','lastName','gender','nationality','language','bio'));
        
    }
}
